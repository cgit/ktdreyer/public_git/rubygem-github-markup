%global gem_name github-markup

# Some tests do string comparisons on sample data. This data can be tightly
# coupled with the exact versions of the interpreter dependencies. Sometimes
# the test data upstream will not match what our software versions in Fedora
# generate. In those cases, we need to regenerate the test data during the RPM
# build process.
%global _regenerate_asciidoc 0
%global _regenerate_lhs 1
%global _regenerate_mediawiki 1
%global _regenerate_pod 1

Name: rubygem-%{gem_name}
Version: 1.0.0
Release: 1%{?dist}
Summary: The code GitHub uses to render README.markup
Group: Development/Languages
License: MIT
URL: https://github.com/github/markup
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Fix for newer RDoc: https://github.com/github/markup/pull/214
Patch0: rubygem-github-markup-1.0.0-rdoc.patch
# TODO: patch out "/usr/bin/env perl" in lib/github/markups.rb
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(github-markdown)
Requires: rubygem(redcarpet)
Requires: rubygem(rdiscount)
Requires: rubygem(maruku)
Requires: rubygem(kramdown)
# The bluecloth interpreter is not yet in Fedora. Comment out for now.
# https://bugzilla.redhat.com/771297
#Requires: rubygem(bluecloth)
Requires: rubygem(RedCloth)
Requires: rubygem(org-ruby)
Requires: rubygem(creole)
Requires: rubygem(wikicloth)
Requires: rubygem(literati)
Requires: rubygem(asciidoctor) >= 0.1.4
Requires: perl(Pod::Simple::HTML)
# The python-creole is not yet in Fedora. Comment out for now.
#Requires: python-creole
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(github-markdown)
BuildRequires: rubygem(redcarpet)
BuildRequires: rubygem(rdiscount)
BuildRequires: rubygem(maruku)
BuildRequires: rubygem(kramdown)
#BuildRequires: rubygem(bluecloth)
BuildRequires: rubygem(RedCloth)
BuildRequires: rubygem(org-ruby)
BuildRequires: rubygem(creole)
BuildRequires: rubygem(wikicloth)
BuildRequires: rubygem(literati)
BuildRequires: rubygem(asciidoctor) >= 0.1.4
BuildRequires: perl(Pod::Simple::HTML)
#BuildRequires: python-creole

BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
This gem is used by GitHub to render any fancy markup such as
Markdown, Textile, Org-Mode, etc.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove developer-only files.
for f in Gemfile Rakefile script/bootstrap script/cibuild; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done
rm -r script

# Fix for newer RDoc. https://github.com/github/markup/pull/214
%patch0 -p1

%if 0%{?_regenerate_asciidoc}
  # Regenerate Asciidoc sample to satisfy tests with our particular version of
  # Asciidoctor.
  pushd test/markups
    # Note: In asciidoctor v0.1.3, the asciidoctor command-line
    # utility will write slightly different output than the equivalent library
    # call!
    # https://github.com/github/markup/pull/235
    # https://bugzilla.redhat.com/1047362
    asciidoctor README.asciidoc
  popd
%endif

%if 0%{?_regenerate_lhs}
  # Regenerate lhs sample to satisfy tests with our particular version of
  # rubygem-literati.
  literati test/markups/README.lhs > test/markups/README.lhs.html
%endif

%if 0%{?_regenerate_mediawiki}
  # Regenerate Mediawiki sample to satisfy tests with our particular version of
  # Wikicloth.
  pushd test/markups
    ruby -rwikicloth -e \
    "puts WikiCloth::Parser.new(:data => IO.read('README.mediawiki')).to_html(:noedit => true)" \
    > README.mediawiki.html
  popd
%endif

%if 0%{?_regenerate_pod}
  # Regenerate POD sample to satisfy tests with our particular version of
  # Pod::Simple::HTML.
  perl -MPod::Simple::HTML -e Pod::Simple::HTML::go test/markups/README.pod \
    | sed  '0,/<!-- start doc -->/d' \
    | sed -n '/<!-- end doc -->/q;p' \
    > test/markups/README.pod.html
%endif

%if 0%{?_regenerate_rst}
  # Regenerate rst sample to satisfy tests with our particular version of
  # rest2html.
  rest2html test/markups/README.rst > test/markups/README.rst.html
  rest2html test/markups/README.rst.txt > test/markups/README.rst.txt.html
%endif

# Some of the test data files just don't line up with what we have in Fedora.
# Skip them entirely.
# litcoffee is totally broken, might be broken upstream as well.
# rst and rst.txt require python-creole, which is not in Fedora.
for f in litcoffee rst rst.txt; do
  rm test/markups/README.$f
  sed -i "s|\"test/markups/README.$f\",||g" %{gem_name}.gemspec
done

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# Remove unnecessary gemspec
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -pa .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%check
pushd .%{gem_instdir}
  testrb -Ilib test/markup_test.rb
popd

%files
%dir %{gem_instdir}
%doc %{gem_instdir}/README.md
%doc %{gem_instdir}/LICENSE
%{_bindir}/github-markup
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/HISTORY.md
%exclude %{gem_instdir}/test

%changelog
* Sat Dec 28 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.0.0-1
- Initial package
